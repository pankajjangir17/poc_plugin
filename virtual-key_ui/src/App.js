import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";
import virtualKeyLogo from './assets/images/logo.png';
import virtualKeyContact from './assets/images/contact_icon.png';
import virtualKeyFaq from './assets/images/faq_icon.png';
import Drawer from '@material-ui/core/Drawer';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';



function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
 
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  }
});

class MenuAppBar extends React.Component {
  state = {
    auth: true,
    anchorEl: null,
    top: false,
    left: false,
    bottom: false,
    right: false,
    open: false
  };
  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };
  handleChange = event => {
    this.setState({ auth: event.target.checked });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    const { classes } = this.props;
    const { auth, anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const sideList = (
      <div className={classes.list}>
        <div className="virtualkey-sidebar mobile">
          <List component="nav">
            <ListItem button className="active virtualkey-sidebar-menu">
              <ListItemIcon>
                <i class="material-icons">home</i>
              </ListItemIcon>
              <ListItemText className="virtualkey-sidebar-text" primary="Dasboard" />
            </ListItem>
            <ListItem button className=" virtualkey-sidebar-menu">
              <ListItemIcon>
                <i class="material-icons">airplanemode_active</i>
              </ListItemIcon>
              <ListItemText className="virtualkey-sidebar-text" primary="My Trips" />
            </ListItem>
            <ListItem button className=" virtualkey-sidebar-menu">
              <ListItemIcon>
                <i class="material-icons">list_alt</i>
              </ListItemIcon>
              <ListItemText className="virtualkey-sidebar-text" primary="My Reservations" />
            </ListItem>
          </List>
          </div>
      </div>
    );
    return (
      <div>
         <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="h5" id="modal-title">
              Conatct Us
            </Typography>
            <form className={classes.container} noValidate autoComplete="off">
            <TextField xs={12}
          id="outlined-email-input"
          label="Email"
          className={classes.textField}
          type="email"
          name="email"
          autoComplete="email"
          margin="normal"
          variant="outlined"
        />
            </form>
          </div>
        </Modal>
        <Drawer className="virtualkey-sidebar-overlay virtualkey-body" open={this.state.left} onClose={this.toggleDrawer('left', false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </Drawer>
        <AppBar className="virtualkey-header" position="static">
          <Toolbar>
           
            <Typography variant="h6" color="inherit" className={classes.grow}>
              <img className="logo" src={virtualKeyLogo} />
              <IconButton onClick={this.toggleDrawer('left', true)} className="virtualkey-header-navicon" color="inherit" aria-label="Menu">
              <i class="material-icons">menu</i>
            </IconButton>
            </Typography>
           
            {auth && (
              <div className="virtualkey-header-infodesktop">
                <Chip
                  avatar={<img className="logo" src={virtualKeyFaq} />}
                  label="User"
                />
                <Chip  onClick={this.handleOpen}
                  avatar={ <img  className="logo" src={virtualKeyContact} />}
                  label="Contact"
                  className={classes.chip}
                />
                <Chip
                  avatar={ <img className="logo" src={virtualKeyFaq} />}
                  label="FAQ"
                  className={classes.chip}
                />

              </div>
              
              
            )}
             {auth && (
              <div className="virtualkey-header-infomobile">
                <Chip
                  avatar={<img className="logo" src={virtualKeyFaq} />}
                />
                <Chip onClick={this.handleOpen}
                  avatar={ <img  className="logo" src={virtualKeyContact} />}
                  className={classes.chip}
                />
                <Chip
                  avatar={ <img className="logo" src={virtualKeyFaq} />}
                  className={classes.chip}
                />

              </div>
              
              
            )}
          </Toolbar>
        </AppBar>
        <Grid className="virtualkey-body collapsable">
        <div className="virtualkey-sidebar desktop">
          <List component="nav">
            <ListItem button className="active virtualkey-sidebar-menu">
              <ListItemIcon>
                <i class="material-icons">home</i>
              </ListItemIcon>
              <ListItemText className="virtualkey-sidebar-text" primary="Dasboard" />
            </ListItem>
            <ListItem button className=" virtualkey-sidebar-menu">
              <ListItemIcon>
                <i class="material-icons">airplanemode_active</i>
              </ListItemIcon>
              <ListItemText className="virtualkey-sidebar-text" primary="My Trips" />
            </ListItem>
            <ListItem button className=" virtualkey-sidebar-menu">
              <ListItemIcon>
                <i class="material-icons">list_alt</i>
              </ListItemIcon>
              <ListItemText className="virtualkey-sidebar-text" primary="My Reservations" />
            </ListItem>
          </List>
          </div>
          
          <div className="virtualkey-container">
            <Grid container spacing={24}>
              <Grid item xs={12} className="viewtrips-container">
                <Typography variant="h5" gutterBottom className="green-text">
                  Welcome to VirtualKEY
      </Typography>
                <select>
                  <option>View All Trips</option>
                  <option>View Trip1</option>
                  <option>View Trip2</option>
                </select>

              </Grid>
              <Grid item xs={12} sm={12} md={4} className="tripinfo-container">
                <Grid item xs={12} className="border-box padding-zero">
                  <Grid item xs={12} align="center" className="tripinfo-image">
                    <img src="https://dummyimage.com/600x200/89c4c4/fff.jpg&text=VirtualKEY" />
                    <List className="address-container">
                      <ListItem>
                        <i class="material-icons">home</i>
                        <ListItemText primary="Lake View Condo" secondary="1234 N Main St" />
                      </ListItem>
                      <li>
                        <Divider  />
                      </li>
                      <ListItem>
                        <i class="material-icons">email</i>
                        <ListItemText primary="info@lakeviewrentals.com" />
                      </ListItem>
                      <li>
                       
                      </li>
                      <ListItem>
                        <i class="material-icons">phone</i>
                        <ListItemText primary="123-456-3456" />
                      </ListItem>
                      <li>
                        
                      </li>
                      <ListItem>
                      <i class="material-icons">exit_to_app</i>
                        <ListItemText primary="123-456-3456" secondary="1234 N Main St" />
                      </ListItem>
                      <i class="material-icons slidericon">
                    keyboard_arrow_down
</i>
                    </List>
                    
                    <img className="onmobile-hide" src="https://dummyimage.com/600x200/89c4c4/fff.jpg&text=VirtualKEY" />
                  </Grid>
                  <Grid item xs={12} align="center" className="padd-15">
                    <Button variant="contained" color="primary" className={`share-btn ${classes.button}`}>
                      Check Out
</Button>
                    <i class="material-icons side-icon-outline">error_outline</i>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} md={8} className="lockdevices-container">
                <Grid item xs={12} className="border-box">
                  <Grid container spacing={24}>
                    <Grid item xs={5} className="low-padding">
                      <AppBar position="static" color="default" className="lock-devices-tabs">
                        <Tabs
                          value={this.state.value}
                          indicatorColor="primary"
                          textColor="primary"
                        >
                          <Tab label="Locks" className="active"/>
                          <Tab  label="Devices" />
                        </Tabs>
                      </AppBar>
                    </Grid>
                    <Grid item xs={7} align="right" className="side-icon-morevert">
                      <Button variant="contained" color="primary" className={`share-btn ${classes.button}`}>
                        Share a VirtualKEY
 <i class="material-icons">share</i>
                      </Button>
                      <i class="material-icons side-icon-outline">error_outline</i>
                      <Select className="ellipsis"
                        IconComponent={props => (
                          <i {...props} className={`material-icons  ${props.className}`}>
                            more_vert
          </i>
                        )}
                        value={this.state.age}
                        onChange={this.handleChange}
                        inputProps={{
                          name: "age",
                          id: "age-simple"
                        }}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </Grid>
                  </Grid>
                 
                  <Grid container className="keycode-container">
                    <Grid xs={12} sm={12} md={8}>
                      <Typography align="left" variant="subtitle1" gutterBottom>
                        Front Door Code
</Typography>
                      <div className="keycode-number">
                        <span>2</span>
                        <span>4</span>
                        <span>6</span>
                        <span>2</span>
                        <span><i class="material-icons tick">
                          done
</i></span>
                      </div>
                      <Typography variant="caption" gutterBottom className="green-text">
                        Enter code on keypad followed by to <i class="material-icons tick">
                          done
</i> Lock and Unlock
</Typography>
                    </Grid>
                    <Grid xs={12} sm={12} md={4} className="btn-top" align="center">
                      <Button variant="contained" color="primary" className={classes.button}>
                        Tap to Unlock
                        <i class="material-icons">lock</i>
      </Button>
                      <div className="devices-icons">
                        <i class="material-icons">bluetooth</i>
                        <i class="material-icons">wifi</i>
                      </div>
                    </Grid>
                  </Grid>
                  <Divider></Divider>
                  <Grid container className="keycode-container">
                    <Grid xs={12} sm={12} md={8}>
                    <Typography align="left" variant="subtitle1" gutterBottom>
                        Back Door Code
</Typography>
                      <div className="keycode-number">
                        <span>2</span>
                        <span>4</span>
                        <span>6</span>
                        <span>2</span>
                        <span><i class="material-icons tick">
                          done
</i></span>
                      </div>
                      <Typography variant="caption" gutterBottom className="green-text">
                        Enter code on keypad followed by to <i class="material-icons tick">
                          done
</i> Lock and Unlock
</Typography>
                    </Grid>
                    <Grid xs={12} sm={12} md={4} className="btn-top" align="center">
                      <Button variant="contained" color="primary" className={classes.button}>
                        Tap to Unlock
                        <i class="material-icons">lock</i>
      </Button>
                      <div className="devices-icons">
                        <i class="material-icons">bluetooth</i>
                        <i class="material-icons">wifi</i>
                      </div>
                    </Grid>
                  </Grid>
                  <Divider></Divider>
                  <Grid container className="keycode-container">
                    <Grid xs={12}>
                    <Typography align="left" variant="subtitle1" gutterBottom>
                        Garage Door Code
</Typography>
                      <div className="keycode-number">
                        <span>2</span>
                        <span>4</span>
                        <span>6</span>
                        <span>2</span>
                        <span>1</span>
                        <span>2</span>
                        <span>4</span>
                        <span>6</span>
                        <span>2</span>
                        <span>1</span>
                      </div>
                      <Typography variant="caption" gutterBottom className="green-text">
                        Enter code on keypad followed by to <i class="material-icons tick">
                          done
</i> Lock and Unlock
</Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </div>
    );
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);



