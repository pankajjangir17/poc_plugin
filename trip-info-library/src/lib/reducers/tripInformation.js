const tripInformation=(state={},action) =>{
    switch(action.type){
        case 'SET_TRIP_DATA':
        return {
            ...state,
            tripData : action.tripData
        }
        case 'SET_WEATHER_DATA':
        return {
            ...state,
            weatherData : action.weatherData
        }
        case 'SET_LOCATION_DATA':
        return {
            ...state,
            locationData : action.locationData
        }
        default:
            return state;
      }
    }
export default tripInformation
    