import React, { Component } from 'react';
import { connect } from 'react-redux'
import weatherIcon from '../media/images/mytripsfor-newdevelopment-download@2x.png'
import lineSeprator from '../media/images/mytripsfor-newdevelopment-line-2.png'
import ovelIcon from '../media/images/mytripsfor-newdevelopment-oval-3@2x.png'
import backgroundTextureImage from '../media/images/mytripsfor-newdevelopment-rectangle-5.png'
import backgroundImage from '../media/images/mytripsfor-newdevelopment-usa-2016mg1638.png'
import '../media/css/styles.css'
import { getTripInfoData, fetchWeatherData } from '../middleware'
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import moment from 'moment'
import { List   } from 'react-content-loader'
import Icon from '@material-ui/core/Icon';

class TripInfoComponent extends Component {
  componentDidMount() {
    this.props.setTripData
    this.props.setWeatherData
    this.ticketDetailTimer = setInterval(this.props.setTripData, 1000);
    this.weatherTimer = setInterval(this.props.setWeatherData, 2000);
  }

  changeLockCode = () => {
      var code = Math.floor(100000 + Math.random() * 900000)
      console.log("code--",code)
      this.props.changeLockCode(code);            
  }

  render() {
    
    const theme = createMuiTheme({
      palette: {
        primary: {      
          main: this.props.color,
        },
        secondary: {
          light: '#0066ff',
          main: '#0044ff',      
          contrastText: '#ffcc00',
        },
        // error: will use the default color
      },
    });
    var tripData = this.props.tripData
    var weatherInfo = this.props.weatherData
    var locationData = this.props.locationData
    var dayTime = moment.unix(weatherInfo.EpochTime).format('dddd h:mm A')
    if (tripData) {
      clearInterval(this.ticketDetailTimer);
    }if(weatherInfo){
      clearInterval(this.weatherTimer);
    }
    
    return (      
      <MuiThemeProvider theme={theme}>
      <div className="group7">
        <div className="group4">
          <div className="rectangle4copy">
          </div>
          
          <img anima-src={weatherIcon} className="download" src={weatherIcon} />
          <img anima-src={backgroundImage} className="usa2016mg1638" src={backgroundImage} />
          <img anima-src={backgroundTextureImage} className="rectangle5" src={backgroundTextureImage} />          
          <img anima-src={lineSeprator} className="line2" src={lineSeprator} />
          <div className="a80">
            {weatherInfo.Temperature === undefined ? "": weatherInfo.Temperature.Metric.Value}
          </div>
          <img anima-src={ovelIcon} className="oval3" src={ovelIcon} />          
          <div className="aug28sept4">
            <div style={{marginRight:75}}>
              <Icon  color="primary">
                  home
              </Icon>
              Lake view condo
            </div>
            <div>
              <Icon  color="primary">
              email
              </Icon>
              info@lakeviewrentals.com
            </div>
            <div style={{marginRight:96}}>
              <Icon  color="primary">
              phone
              </Icon>
              988-909-8976
            </div>
            <div>
              <Icon  color="primary">
              exit_to_app
              </Icon>
              check in : Aug 28, 2:00 PM
            </div>              
            
          </div>          
          <div className="checkin200pm">
            <span className="span1">Check In:</span><span className="span2">{tripData.tripCountry}</span>
            <div className="ck-button">                          
                <Button color="primary" variant="contained" onClick={() => this.changeLockCode()}>Check Out</Button>                               
            </div>
          </div>
          <div className="checkout1145pm">
            <span className="span1">Check Out:</span><span className="span2">{tripData.tripCountry}</span>
          </div>
          <div className="group8copy">
            <div className="rectangle">
            </div>
            <div className="viewalltrips">
              View All Trips
                </div>
            <div className="label1">
              &gt;
                </div>
                <div>
                  {weatherInfo && locationData ? 
                  (
                    <div>
                      <div className="tp-info-City">
                        {locationData === undefined ? "" : locationData.EnglishName}, {locationData === undefined ? "" : locationData.Country === undefined ? "": locationData.Country.LocalizedName}
                      </div>
                      <div className="tp-date-time">
                        {dayTime}
                      </div>
                  </div>
                  )
                    : (
                      <div className="tp-info-loader">
                        <List /> 
                      </div>
                    )}
                </div>
          </div>
        </div>        
      </div>
      </MuiThemeProvider>
    );
    
  }
}
const mapStateToProps = (state) => ({
  tripData: state.tripInformation === undefined ? '' : (state.tripInformation.tripData === undefined ? '' : state.tripInformation.tripData),
  weatherData : state.tripInformation === undefined ? '' : (state.tripInformation.weatherData === undefined ? '' : state.tripInformation.weatherData),
  locationData : state.tripInformation === undefined ? '' : (state.tripInformation.locationData === undefined ? '' : state.tripInformation.locationData)
})

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setTripData: () => dispatch(getTripInfoData()),
    setWeatherData: () => dispatch(fetchWeatherData())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TripInfoComponent)