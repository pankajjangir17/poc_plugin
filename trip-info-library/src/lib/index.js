import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TripInfoComponent from './components/TripInfoComponent'
import {Provider} from 'react-redux';
import {store} from './store'
export default class TripInformationComponent extends Component {
  render() {  
    return (
      <Provider store={store}>
        <TripInfoComponent 
          changeLockCode={this.props.changeLockCode}
          color={this.props.color}
        />
    </Provider>      
    )
  }
}