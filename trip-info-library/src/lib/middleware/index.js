import { setTripData, setWeatherData, setLocationData } from '../actions'
import axios from 'axios'

export function getTripInfoData() {
	return (dispatch, getState) => {
		var tripData = {
			"tripCountry":"Chicago",
			"tripCity": "Lake View Condo",
			"tripAddress":"1232 N Main St",
			"tripCheckInDate":"28-Aug-2018",
			"tripCheckInTime":"04:00 PM",
			"tripCheckoutDate":"04-Sep-2018",
			"tripCheckoutTime":"11:00 AM",
			"eSignDocumentRequired":true,
			"eSignStatus":"Done"
		}; 
		dispatch(setTripData(tripData))
		// axios.get('/user?ID=12345')
		// 	.then(function (response) {
		// 		console.log(response);
		// 	})
		// 	.catch(function (error) {
		// 		console.log(error);
		// 	});
	}
}

export function updateLockCode(){
	return (dispatch, getState) => {
		var state = getState();
		console.log("state data is",state)
	}
}

export function fetchWeatherData(){
	return (dispatch) => {
		axios.get('http://dataservice.accuweather.com/locations/v1/cities/geoposition/search.json?q=28.61,77.20&apikey=Aw9n2GZzCoMI4rXqqppIZlC0gyTRje0I')
			.then(response=>{
				if(response.status === 200 || response.status === "OK"){
					dispatch(setLocationData(response.data))
					axios.get('http://dataservice.accuweather.com/currentconditions/v1/'+response.data.Key+'?apikey=Aw9n2GZzCoMI4rXqqppIZlC0gyTRje0I&language=en-in&details=false')
					.then(response=>{						
						if(response.status === 200 || response.status === "OK"){							
							dispatch(setWeatherData(response.data[0]))
						}
					})					
				}
			})
			.catch(function (error) {
				console.log("error is ",error)
			});
	}
}