export const setTripData=(tripData)=>{
	return{
    	type:'SET_TRIP_DATA',
        tripData        
	}
}

export const setWeatherData = (weatherData)=>{
	return {
		type : "SET_WEATHER_DATA",
		weatherData
	}
}
export const setLocationData = (locationData)=>{
	return {
		type : 'SET_LOCATION_DATA',
		locationData
	}
}