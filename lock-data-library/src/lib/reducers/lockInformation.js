const lockInformation=(state={},action) =>{
    switch(action.type){
        case 'SET_LOCK_DATA':
        return {
            ...state,
            lockData : action.lockData
        }
        default:
            return state;
      }
    }
export default lockInformation
    