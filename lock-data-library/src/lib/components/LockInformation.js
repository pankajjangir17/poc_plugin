import React, { Component } from 'react';
import { connect } from 'react-redux'
import batteryIcon from '../media/images/mytripsfor-newdevelopment-473-512-copy@2x.png'
import lockIcon from '../media/images/mytripsfor-newdevelopment-bitmap.png'
import wifiIcon from '../media/images/mytripsfor-newdevelopment-black-wifi-icon-md@2x.png'
import unlockButton from '../media/images/mytripsfor-newdevelopment-group-13@2x.png'
import smallLock from '../media/images/mytripsfor-newdevelopment-iconlock@2x.png'
import sepratorLine from '../media/images/mytripsfor-newdevelopment-line-2-copy.png'
import '../media/css/styles.css'
import { getLockInfoData } from '../middleware'
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ShareVirtualKeyComponent from 'sharekey-form-library'

const options = [
  'None',
  'Atria',
  'Callisto',
  'Dione'
];
const ITEM_HEIGHT = 48;
function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

class LockInformation extends Component {
  state = {
    anchorEl: null,
  };
  componentDidMount() {
    this.props.setLockData
    this.lockDetailTimer = setInterval(this.props.setLockData, 1000);
  }
  showModal = () => {
    this.props.openModal();
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleCloseOption = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const theme = createMuiTheme({
      palette: {
        primary: {
          main: this.props.color,
        },
        secondary: {
          light: '#0066ff',
          main: '#0044ff',
          contrastText: '#ffcc00',
        },
        // error: will use the default color
      },
    });
    if (this.props.lockData) {
      clearInterval(this.lockDetailTimer);
    }
    var lockCode = this.props.lockCode === "" ? this.props.lockData : this.props.lockCode
    console.log("this props---", this.props.lockCode)
    return (
      <MuiThemeProvider theme={theme}>
      <div className="group6">
        <div className="group5copy">
          <div className="rectangle4copy">
          </div>
          <img anima-src={sepratorLine} className="line2" src={sepratorLine} />
          <img anima-src={sepratorLine} className="line2copy" src={sepratorLine} />
          <img anima-src={sepratorLine} className="line2copy2" src={sepratorLine} />
        </div>
        <div className="frontdoor">
          Front Door
        </div>
        <div className="backdoor">
          Back Door
        </div>
        <div className="garagedoor">
          Garage Door
        </div>
        <div className="forsecurityreasons">
          <span className="span1">For security reasons, your host recommeds that you </span><span className="span2">DO NOT</span><span className="span3"> share your <br />digital keys/ codes with other guests in the group. Instead, send them their <br />own access by using </span><span className="span4">SHARE A VIRTUAL KEY</span><span className="span5">. Create as many keys as you <br />like, its safe and free!</span>
        </div>
        <div className="entercodeonkeypad">
          Enter code on keypad followed by To Lock/Unlock
      </div>
        <div className="code">
          Code
      </div>
        <div className="reservationfromag">
          <span className="span1">Reservation From: </span><span className="span2">AGOSA</span><span className="span3"> </span>
        </div>
        <div className="confirmationcode2">
          <span className="span1">Confirmation Code: </span><span className="span2">2194580 </span>
        </div>
        <div className="group5" style={{ "font-size": "2em" }}>
          {lockCode.lockCode}
        </div>
        <img anima-src={wifiIcon} className="blackwifiiconmd" src={wifiIcon} />
        <img anima-src={batteryIcon} className="a473512copy" src={batteryIcon} />
        <img anima-src={lockIcon} className="bitmap1" src={lockIcon} />
        <img anima-src={unlockButton} className="group13" src={unlockButton} />
        <div className="share-key-btn">
          
            <ShareVirtualKeyComponent.default />
          
        </div>
        <img anima-src={smallLock} className="iconlock" src={smallLock} />
        <div className="elips">
          <IconButton
            aria-label="More"
            aria-owns={open ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}
          >
            <MoreVertIcon />
          </IconButton>
          <Menu
            id="long-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={this.handleCloseOption}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: 200,
              },
            }}
          >
            {options.map(option => (
              <MenuItem key={option} selected={option === 'Pyxis'} onClick={this.handleCloseOption}>
                {option}
              </MenuItem>
            ))}
          </Menu>
        </div>
        <div className="rectangle6">
        </div>
      </div>
      </MuiThemeProvider>
    );
  }
}
const mapStateToProps = (state) => ({
  lockData: state.lockInformation === undefined ? '' : (state.lockInformation.lockData === undefined ? '' : state.lockInformation.lockData)
})

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setLockData: () => dispatch(getLockInfoData())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LockInformation)