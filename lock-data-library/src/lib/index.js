import React, { Component } from 'react'
import PropTypes from 'prop-types'
import LockInformation from './components/LockInformation'
import {Provider} from 'react-redux';
import {store} from './store'
export default class LockComponent extends Component {
  render() {  
    return (
      <Provider store={store}>
        <LockInformation 
          openModal={this.props.openModal} 
          lockCode={this.props.lockCode}
          buttonColor={this.props.buttonColor}
          color={this.props.color}
        />
    </Provider>      
    )
  }
}