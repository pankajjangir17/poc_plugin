import React from 'react';
import ReactDOM from 'react-dom';
import  MainApp  from '../lib/components/LockInformation';
import {Provider} from 'react-redux';
import {store} from '../lib/store'
const App = () => (
  <div>
    <MainApp text="Click me!" />    
  </div>
);

ReactDOM.render(
  <Provider store={store}>
        <App />
    </Provider> 
, 
document.getElementById('root'));
